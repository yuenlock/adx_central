# frozen_string_literal: true

require_relative './core/adx_rails/adx_model'
require_relative './core/adx_rails/api_controller'
