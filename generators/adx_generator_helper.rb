# require 'generators/generator_base'

module AdxGeneratorHelper
  CRUD_ACTIONS = %w[create index show update destroy].freeze
  CUD_ACTIONS = %w[create update destroy].freeze

  private

  def mylog(str)
    puts '        ' + str
  end

  def mylog1(str)
    mylog '  ' + str
  end

  def spec_file_name(fn)
    "spec/#{fn.sub('.rb', '_spec.rb')}"
  end

  def attributes_string
    attributes.map { |a| "#{a.name}:#{a.type}" }.join ' '
  end
end
