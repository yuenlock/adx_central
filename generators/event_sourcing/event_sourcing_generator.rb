# frozen_string_literal: true

require 'generators/adx_generator_helper'

class EventSourcingGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('templates', __dir__)

  include AdxGeneratorHelper

  def create_event_files
    mylog 'Generating Event Files'
    file_path = ['components', class_path, 'events', file_name.pluralize]

    CUD_ACTIONS.each do |action|
      full_file_name = File.join(*file_path, "#{action}.rb")
      mylog1 full_file_name
    end

    full_file_name = File.join('components', class_path, 'events', file_name.pluralize, "#{file_name}_handler")
    mylog1 full_file_name

    full_file_name = File.join('components', class_path, 'events', file_name.pluralize, "#{file_name}_listener")
    mylog1 full_file_name
  end

end
