# frozen_string_literal: true

require 'rails_helper'
<% test_class = class_name.demodulize.underscore.singularize -%>
RSpec.describe <%= class_name_plural.sub('::', '::Api::V1::') -%>Controller, type: :controller do
  let(:results_from_repo) {
    [
      { label: 'Katarina', name: 'Katarina - Blades all around' },
      { label: 'MsFortune', name: 'Miss Fortune - Bullets all around' }
    ]
  }
  let(:serializer_options) { { host: Rails.configuration.host_path } }

  before(:each) do
    request.headers['Authorization'] = 'Token XXYY111'
  end

  describe 'Create' do
    let(:service_class) { <%= class_name_plural.sub('::', '::Services::') -%>::Create }
    let(:serializer_class) { <%= class_name_plural.sub('::', '::Api::V1::Serializers::') -%>::Create }
    let(:attrs) { { guid: SecureRandom.hex(16), label: 'Morgana' } }
    let(:create_params) { { <%= test_class -%>: attrs } }

    let(:create_service_params) {
      {
        '<%= test_class -%>' => ActionController::Parameters.new(attrs),
        controller: described_class.name.gsub('::', '/').gsub('Controller', '').underscore,
        action: 'create'
      }
    }

    it 'response as json with status 200' do
      expect(service_class).to receive(:call) { { guid: attrs[:guid] } }
      expect(serializer_class).to receive(:call)
      post :create, params: create_params
      expect(response.status).to eq 200
      expect(response.content_type).to eq 'application/json'
    end

    it 'calls service object with proper params' do
      expect(service_class).to(
        receive(:call).with(params: ActionController::Parameters.new(create_service_params), attrs: attrs)
                      .and_return(guid: attrs[:guid])
      )
      expect(serializer_class).to receive(:call)
      post :create, params: create_params
    end

    it 'calls serializer with service_object result' do
      expect(service_class).to(
        receive(:call).with(params: ActionController::Parameters.new(create_service_params), attrs: attrs)
                      .and_return(guid: create_params[:guid])
      )
      expect(serializer_class).to receive(:call).with(member: { guid: create_params[:guid] }, errors: [])
      post :create, params: create_params
    end
  end

  describe 'Update' do
    let(:service_class) { <%= class_name_plural.sub('::', '::Services::') -%>::Update }
    let(:serializer_class) { <%= class_name_plural.sub('::', '::Api::V1::Serializers::') -%>::Update }

    let(:update_params) { { id: attrs[:guid], <%= class_name.demodulize.singularize.underscore -%>: attrs } }

    let(:attrs) { { guid: SecureRandom.hex(16), label: 'Katarina The Quick' } }

    let(:update_service_params) {
      {
        '<%= test_class -%>' => ActionController::Parameters.new(attrs),
        guid: attrs[:guid],
        controller: described_class.name.gsub('::', '/').gsub('Controller', '').underscore,
        action: 'update'
      }
    }


    it 'response as json with status 200' do
      expect(service_class).to receive(:call) { { guid: attrs[:guid], errors: [] } }
      expect(serializer_class).to receive(:call)
      patch :update, params: update_params
      expect(response.status).to eq 200
      expect(response.content_type).to eq 'application/json'
    end

    it 'calls service object with proper params' do
      expect(service_class).to(
        receive(:call).with(params: ActionController::Parameters.new(update_service_params), attrs: attrs)
                      .and_return(guid: attrs[:guid], errors: [])
      )
      expect(serializer_class).to receive(:call)
      patch :update, params: update_params
    end

    it 'calls serializer with service_object result' do
      expect(service_class).to receive(:call).and_return(guid: attrs[:guid], errors: [])
      expect(serializer_class).to receive(:call).with(member: { guid: attrs[:guid], errors: [] }, errors: [])
      patch :update, params: update_params
    end
  end

  describe 'Index' do
    let(:service_class) { <%= class_name_plural.sub('::', '::Services::') -%>::Index }
    let(:serializer_class) { <%= class_name_plural.sub('::', '::Api::V1::Serializers::') -%>::Index }

    let(:std_params) { { label: 'New Abc' } }

    let(:index_service_params) {
      std_params.merge(
        controller: described_class.name.gsub('::', '/').sub('Controller', '').underscore,
        'action': 'index'
      )
    }

    it 'response as json with status 200' do
      expect(service_class).to receive(:call)
      expect(serializer_class).to receive(:call)
      get :index, params: std_params
      expect(response.status).to eq 200
      expect(response.content_type).to eq 'application/json'
    end

    it 'calls service object with proper params' do
      expect(service_class).to receive(:call).with(params: index_service_params).and_return(results_from_repo)
      expect(serializer_class).to receive(:call)
      get :index, params: std_params
    end

    it 'calls serializer with service_object result' do
      expect(service_class).to receive(:call).and_return(results_from_repo)
      expect(serializer_class).to(
        receive(:call).with(
          collection: results_from_repo, options: serializer_options,
          params: ActionController::Parameters.new(index_service_params)
        )
      )
      get :index, params: std_params
    end
  end

  describe 'Show' do
    let(:service_class) { <%= class_name_plural.sub('::', '::Services::') -%>::Show }
    let(:serializer_class) { <%= class_name_plural.sub('::', '::Api::V1::Serializers::') -%>::Show }

    let(:std_params) { { id: 'abcd1234' } }

    let(:show_service_params) {
      {
        guid: std_params[:id],
        controller: described_class.name.gsub('::', '/').sub('Controller', '').underscore,
        action: 'show'
      }
    }

    it 'response as json with status 200' do
      expect(service_class).to receive(:call).and_return(results_from_repo[0])
      expect(serializer_class).to receive(:call)
      get :show, params: std_params
      expect(response.status).to eq 200
      expect(response.content_type).to eq 'application/json'
    end

    it 'calls service object with proper params' do
      expect(service_class).to receive(:call).with(params: show_service_params) { results_from_repo[0] }
      expect(serializer_class).to receive(:call)
      get :show, params: std_params
    end

    it 'calls serializer with service_object result' do
      expect(service_class).to receive(:call) { results_from_repo[0] }
      expect(serializer_class).to(
        receive(:call).with(
          member: results_from_repo[0], errors: [], options: serializer_options,
          params: ActionController::Parameters.new(show_service_params)
        )
      )
      get :show, params: std_params
    end
  end
end
