# frozen_string_literal: true

require './components/<%= class_name.sub('::', '::Services::').sub('::', '/').pluralize.underscore %>/index'

RSpec.describe <%= class_name.sub('::', '::Services::').pluralize -%>::Index do
  let(:std_options) { { some: 'thing' } }
  let(:std_params) { { filter: 'abc' } }

  let(:expected_result) { [{ a: 1 }, { b: 2 }] }

  describe '.call' do
    subject { described_class.call(params: std_params, options: std_options) }
    let(:instance) { instance_double(described_class, process: expected_result) }

    it 'calls new and process' do
      expect(described_class).to receive(:new).with(params: std_params, options: std_options) { instance }
      expect(instance).to receive(:process)
      expect(subject).to eq(expected_result)
    end
  end

  describe '#process' do
    let(:repo_class) {
      Class.new(Object) do
        def initialize(options: {}) end

        def retrieve_all
          [{ a: 1 }, { b: 2 }]
        end
      end
    }
    let(:expected_result) { [{ a: 1 }, { b: 2 }] }

    subject { described_class.new(params: std_params, options: std_options, repository: repo_class) }

    it 'calls' do
      expect(subject.repository).to eq repo_class
      expect(subject.process).to eq(expected_result)
    end
  end
end
