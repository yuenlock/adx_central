# frozen_string_literal: true

require 'generators/adx_generator_helper'

class ApiResourceGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('templates', __dir__)
  include AdxGeneratorHelper

  class_option :attributes, type: :string, default: []
  class_option :version, type: :string
  class_option :services, type: :boolean

  def create_controller_file
    mylog 'Generating Controller'
    full_file_name = File.join("components", class_path, 'api', "v#{options[:version]}", "#{file_name.pluralize}_controller.rb")
    mylog1 full_file_name
    template('controller.rb', full_file_name) unless File.exist? full_file_name
    api_version_controller_name = File.join("components", class_path, 'api', "v#{options[:version]}", "api_v#{options[:version]}_controller.rb")
    template('versioned_controller.rb', api_version_controller_name) unless File.exist?(api_version_controller_name)
    template 'spec/controller_spec.rb', spec_file_name(full_file_name) unless File.exist?(spec_file_name(full_file_name))
  end

  def create_serializer_files
    mylog 'Generating Serializers'
    file_path = ['components', class_path, 'api', "v#{options['version']}", 'serializers', file_name.pluralize]

    CRUD_ACTIONS.each do |action|
      full_file_name = File.join(*file_path, "#{action}.rb")
      mylog1 full_file_name
      @action_name = action
      template "serializers/#{action}_serializer.rb", full_file_name unless File.exist?(full_file_name)
      template "spec/serializers/#{action}_serializer_spec.rb", spec_file_name(full_file_name) unless File.exist?(spec_file_name(full_file_name))
    end
  end

  def create_services_files
    return unless options[:services]

    mylog 'Generating Services'
    file_path = ['components', class_path, 'services', file_name.pluralize]

    CRUD_ACTIONS.each do |action|
      full_file_name = File.join(*file_path, "#{action}.rb")
      mylog1 full_file_name
      @action_name = action
      template("services/#{action}_service.rb", full_file_name) unless File.exist? full_file_name

      template "spec/services/#{action}_spec.rb", spec_file_name(full_file_name) unless File.exist? spec_file_name(full_file_name)
    end

    %w[create update].each do |action|
      full_file_name = File.join(*file_path, "#{action}_validator.rb")
      mylog1 full_file_name
      @action_name = action
      template "services/#{action}_validator.rb", full_file_name unless File.exist? full_file_name
      template "spec/services/#{action}_validator_spec.rb", spec_file_name(full_file_name) unless File.exist? spec_file_name(full_file_name)
    end
  end

  def append_route
    mylog 'Generating Routes'
    full_file_name = File.join("components", class_path, 'routes.rb')
    template('routes.rb', full_file_name) unless File.exist? full_file_name

    # add the version name space
    version_name_space_inserted = insert_into_file(
      full_file_name,
      "#{'  ' * 6}namespace :v#{options[:version]}, format: 'json' do\n",
      after: "namespace :api do\n"
    )

    puts 'inserted namespace' if version_name_space_inserted
    insert_into_file(
      full_file_name,
      "#{'  ' * 6}end\n",
      after: "#{'  ' * 6}namespace :v#{options[:version]}, format: 'json' do\n",
      force: true
    ) if version_name_space_inserted

    # add the route
    insert_into_file(
      full_file_name,
      "#{'  ' * 7}resources :#{file_name.pluralize}, only: %i[create index show update]\n",
      after: "namespace :v#{options[:version]}, format: 'json' do\n",
      force: true
    )

    template('spec/routes_spec.rb', spec_file_name(full_file_name)) unless File.exist? spec_file_name(full_file_name)
  end

  private

  def attributes
    options[:attributes].split(',').map { |a| a.split(':').first }
  end

  def component_module
    @component_module ||= class_path.first.camelize
  end

  def component_name
    @component_name ||= class_path.first.underscore
  end

  def class_name_plural
    @class_name_plural ||= class_name.pluralize
  end

  def spec_file_name(full_file_name)
    File.join('spec', full_file_name.sub('.rb', '_spec.rb'))
  end
end
