# frozen_string_literal: true

require './components/<%= (class_name.sub('::', '::Commands::').sub('::', '/').pluralize + "/#{@action_name}").underscore %>'

RSpec.describe <%= class_name.sub('::', '::Commands::').pluralize + "::#{@action_name.camelize}" -%> do
  describe '.call' do
    it 'works'
  end
end
