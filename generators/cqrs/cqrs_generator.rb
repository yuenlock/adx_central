# frozen_string_literal: true

require 'generators/adx_generator_helper'

class CqrsGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('templates', __dir__)

  include AdxGeneratorHelper

  class_option :attributes, type: :string, default: []

  def create_command_files
    mylog 'Generating Commands'
    file_path = ['components', class_path, 'commands', file_name.pluralize]

    CUD_ACTIONS.each do |action|
      full_file_name = File.join(*file_path, "#{action}.rb")
      mylog1 full_file_name
      @action_name = action
      template("#{action}.rb", full_file_name)
      template("spec/command_spec.rb", spec_file_name(full_file_name)) unless File.exists? spec_file_name(full_file_name)
    end
  end

  def create_query_file
    mylog 'Generating Query'
    full_file_name = File.join('components', class_path, 'queries', "#{file_name.singularize}.rb")
    mylog1 full_file_name
    template('query.rb', full_file_name) unless File.exists? full_file_name
    template('spec/query_spec.rb', spec_file_name(full_file_name)) unless File.exists? spec_file_name(full_file_name)
  end

  def create_repository_file
    mylog 'Generating Repository'
    full_file_name = File.join('components', class_path, 'repositories', "#{file_name.pluralize}.rb")
    mylog1 full_file_name
    template('repository.rb', full_file_name) unless File.exists? full_file_name
    template('spec/repository_spec.rb', spec_file_name(full_file_name)) unless File.exists? spec_file_name(full_file_name)
  end

  private

  def attributes
    options[:attributes].split(',').map { |a| a.split(':').first }
  end
end
