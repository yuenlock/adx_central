require 'generators/adx_generator_helper'

class ComponentGenerator < Rails::Generators::NamedBase
  include AdxGeneratorHelper
  CRUD_ACTIONS = %w[create index show update destroy].freeze
  CUD_ACTIONS = %w[create update destroy].freeze

  source_root File.expand_path('templates', __dir__)

  argument :attributes, type: :array, default: [], banner: "field[:type][:index] field[:type][:index]"
  class_option :version, type: :string, default: '1'
  class_option :services, type: :boolean, default: false
  class_option :skip_model, type: :boolean, default: false
  class_option :parent, type: :string

  def generate_api_resource
    generate(:api_resource, name, "--attributes=#{attributes_string.gsub(' ', ',')} --version=#{options[:version]} --services=options[:services]")
  end

  def generate_cqrs
    generate('cqrs', name, "--attributes=#{attributes_string.gsub(' ', ',')}")
  end

  def generate_model
    generate('model', name, attributes_string) unless options[:skip_model]
  end

  # def create_command_files
  #   log 'Generating Commands'
  #   file_path = ['components', class_path, 'commands', file_name.pluralize]

  #   CUD_ACTIONS.each do |action|
  #     full_file_name = File.join(*file_path, "#{action}.rb")
  #     log1 full_file_name
  #   end
  #   # CUD_ACTIONS.each do |action|
  #   #   full_file_name = File.join(*file_path, "#{action}_validator.rb")
  #   #   log1 full_file_name
  #   # end
  # end

  def create_event_files
    # generate(:event_sourcing, name)
  end


  private

  # def log(str)
  #   puts str
  # end

  # def log1(str)
  #   log '  ' + str
  # end
end

