# frozen_string_literal: true

require 'active_support/core_ext/string/inflections'
require 'active_support/hash_with_indifferent_access'

require_relative 'core/events'
require_relative 'core/services'
require_relative 'core/utilities'
