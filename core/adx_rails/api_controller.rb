# frozen_string_literal: true

module AdxCentral
  class ApiController < ActionController::Base
    protect_from_forgery unless: -> { request.format.json? }
    skip_before_action :verify_authenticity_token

    before_action :replace_id_with_guid, only: %i[show update create]

    def index
      result = service_name.constantize.call(params: allowed_params_for_index)
      render json: serializer_name.constantize.call(collection: result, params: params, options: { host: host })
    end

    def show
      result = service_name.constantize.call(params: allowed_params_for_show)
      render json: serializer_name.constantize.call(member: result, errors: result.fetch(:errors, []), params: params, options: { host: host })
    end

    def create
      result = service_name.constantize.call(params: params, attrs: attrs_for_create)
      render json: serializer_name.constantize.call(member: result, errors: result.fetch(:errors, []))
    end

    def update
      result = service_name.constantize.call(params: params, attrs: attrs_for_update)
      render json: serializer_name.constantize.call(member: result, errors: result.fetch(:errors, []))
    end

    def destroy
      result = service_name.constantize.call(params: params, attrs: attrs_for_destroy)
      render json: serializer_name.constantize.call(member: result, errors: result.fetch(:errors, []))
    end

    def genus_name
      self.class.name.demodulize.sub('Controller', '')
    end

    def service_name
      self.class.name.sub('Api::V1::', 'Services::').sub('Controller', "::#{params[:action].camelize}")
    end

    def serializer_name
      self.class.name.sub("::#{genus_name}", "::Serializers::#{genus_name}::").sub('Controller', params[:action].camelize)
    end

    cattr_reader :allowed_params, :allowed_update_params

    def self.allow_params(allowed_keys_hash)
      @@allowed_params ||= {
        create: %i[controller action format guid],
        update: %i[controller action format guid],
        show:   %i[controller action format guid],
        index:  %i[controller action format],
        destroy: %i[controller action format guid]
      }

      allowed_keys_hash.each do |k, v|
        @@allowed_params[k] = (@@allowed_params[k] + v).uniq
      end
    end

    def host
      Rails.configuration.host_path
    end

    def genus_symbol
      genus_name.singularize.underscore.to_sym
    end

    def allowed_params_for_index
      params.permit(allowed_params[:index]).to_h
    end

    def allowed_params_for_show
      params.permit(allowed_params[:show]).to_h
    end

    def attrs_for_create
      sanitized_params_hash(allowed_params[:create])
    end

    def attrs_for_update
      sanitized_params_hash(allowed_params[:update])
    end

    def attrs_for_destroy
      sanitized_params_hash(allowed_params[:destroy])
    end

    def sanitized_params_hash(allowable_param_keys)
      params.require(genus_symbol).permit(*allowable_param_keys).to_h
    end

    def replace_id_with_guid
      params[:guid] = params.delete(:id) if params.key? :id
    end
  end
end
