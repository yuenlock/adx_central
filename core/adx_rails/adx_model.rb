# frozen_string_literal: true

module AdxCentral
  class AdxModel < ActiveRecord::Base
    self.abstract_class = true
  end
end
