# frozen_string_literal: true

require_relative './jwt_encoder'

module AdxCentral
  class TokenAuthenticator
    class << self
      def call(auth_string, authenticator: nil)
        token_type, token = auth_string.split
        (authenticator || authenticator_for(token_type))&.decode(token)
      end

      def authenticator_for(token_type)
        "AdxCentral::#{token_type.sub('Token', '')}Encoder".safe_constantize
      end
    end
  end
end
