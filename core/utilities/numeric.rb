# frozen_string_literal: true

class Numeric
  def to_duration
    secs  = to_int
    mins  = secs / 60
    hours = mins / 60
    # days  = hours / 24

    # if days.positive?
    #   "#{days} days #{hours % 24}:#{format('%02d', mins % 60)}"
    if hours.positive?
      "#{format('%02d', hours)}:#{format('%02d', mins % 60)}"
    elsif mins.positive?
      "00:#{format('%02d', mins)}"
    elsif secs.positive?
      "#{secs} seconds"
    end
  end
end
