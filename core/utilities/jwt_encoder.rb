# frozen_string_literal: true

module AdxCentral
  class JwtEncoder
    class << self
      def decode(token, secret: secret_key, decoder: JWT)
        decoded_hash = decoder.decode(token, secret)

        HashWithIndifferentAccess.new(decoded_hash[0]) if decoded_hash
      rescue decoder::VerificationError, decoder::DecodeError
        {}
      end

      def encode(payload, secret: secret_key, encoder: JWT, expires_in: 1.month)
        full_payload = payload.merge(expiry: (Time.now + expires_in).to_s)
        encoder.encode(full_payload, secret)
      end

      private

      def secret_key
        Rails.application.secrets[:secret_key_base]
      end
    end
  end
end
