# frozen_string_literal: true

module AdxCentral
  module SerializerUtils
    def sub_hash(parent_entity, child_symbol, entity_klass = nil)
      entity_klass ||= child_symbol.to_s.pluralize
      {
        child_symbol => parent_entity[child_symbol].merge(url_for(entity_class: entity_klass, guid: parent_entity[child_symbol][:guid]))
                                                   .except(:references)
      }
    end
  end
end
