# frozen_string_literal: true

module AdxCentral
  module UrlUtils
    def url_for(entity_class:, guid:)
      build_url_for(
        res_guid: guid, controller: params[:controller],
        entity_class: entity_class, host: options[:host]
      )
    end

    def build_url_for(res_guid:, controller:, host:, entity_class:, api_path: nil)
      { url: "#{host}#{api_path || build_api_path(controller, entity_class)}/#{res_guid}.json" }
    end

    def build_api_path(controller, entity_class)
      (controller.split('/')[1..-2] + [entity_class.pluralize]).join('/')
    end
  end
end
