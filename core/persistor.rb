# frozen_string_literal: true

module AdxCentral
  class Persistor
    def self.call(aggregate:, guid:, event_name:, data:, meta_data:)
      new(aggregate: aggregate, guid: guid, event_name: event_name, data: data, meta_data: meta_data).process
    end

    attr_reader :aggregate, :guid, :event_name, :data, :meta_data, :event_model
    def initialize(aggregate:, guid:, event_name:, data:, meta_data:, event_model: nil)
      @aggregate = aggregate
      @guid = guid
      @event_name = event_name
      @data = data
      @meta_data = meta_data
      @event_model = event_model || AdxCentral::Models::AdxEvent
    end

    def process
      event_model.create(aggregate: aggregate, guid: guid, name: event_name, data: data.to_json, meta_data: meta_data.to_json)
    end
  end
end
