# frozen_string_literal: true

module AdxCentral
  class ServiceIndex
    def self.call(params:, options: nil)
      new(params: params, options: options).process
    end

    attr_reader :params, :options, :params_repository

    def initialize(params:, options: {}, repository: nil)
      @params = params
      @options = options
      @params_repository = repository
    end

    def self.repository(repo_class)
      @custom_repository = repo_class
    end

    def process
      respond_to?(:retrieve) ? retrieve : retrieve_all
    end

    def retrieve_all
      repository.new(options: params.slice(:controller, :action)).retrieve_all
    end

    # private

    def repository
      params_repository || (custom_repository || default_repository).constantize
    end

    def custom_repository
      self.class.instance_variable_get(:@custom_repository)
    end

    def default_repository
      self.class.name.sub('Services', 'Repositories').sub('::Index', '')
    end
  end
end
