# frozen_string_literal: true

module AdxCentral
  class ServiceCreateEvent < ServiceCreate
    class << self
      def default_event_recorder
        name.sub('Services', 'Events')
      end
    end
  end
end
