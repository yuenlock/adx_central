# frozen_string_literal: true

module AdxCentral
  class ServiceShow
    def self.call(params:, options: {})
      new(params: params, options: options).process
    end

    attr_reader :params, :options, :params_repository

    def initialize(params:, options: {}, repository: nil)
      @params = params
      @options = options
      @params_repository = repository
    end

    def self.repository(repo_class)
      @custom_repository = repo_class
    end

    def retrieve
      repository.new(options: params.slice(:controller, :action))
                .find_by_guid(params[:guid])
      # raise NotImplementedError,
      #       "Implement his in  #{self.class.name}\n" \
      #       "  def retrieve\n" \
      #       "    repository.retrieve(guid: params[:guid])\n"  \
      #       '  end'
    end

    def process
      retrieve || { errors: { guid: ['is invalid.'] } }
    end

    private

    def repository
      params_repository || (custom_repository || default_repository).constantize
    end

    def custom_repository
      self.class.instance_variable_get(:@custom_repository)
    end

    def default_repository
      self.class.name.sub('Services', 'Repositories').sub('::Show', '')
    end
  end
end
