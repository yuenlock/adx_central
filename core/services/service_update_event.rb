# frozen_string_literal: true

module AdxCentral
  class ServiceUpdateEvent < ServiceUpdate
    class << self
      def default_event_recorder
        # '<Component>::Events::<Genus>::Create'
        name.gsub('Services', 'Events')
      end
    end
  end
end
