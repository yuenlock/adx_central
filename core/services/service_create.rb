# frozen_string_literal: true

module AdxCentral
  class ServiceCreate
    def self.call(params:, attrs:, options: {})
      new(params: params, attrs: attrs, options: options).process
    end

    attr_reader :params, :attrs, :options, :injected_event_recorder, :injected_validator

    def initialize(params:, attrs:, options: {}, event_recorder: nil, validator: nil)
      @params = params
      @options = options
      @attrs = attrs
      @injected_event_recorder = event_recorder
      @injected_validator = validator
    end

    def process
      valid? ? process_request : { errors: errors }
    end

    # override validate and errors together
    def validate
      validation_result.errors.empty?
      # raise NotImplementedError,
      #       "Implement this in  #{self.class.name}\n" \
      #       "  def validate\n" \
      #       "    (attrs.keys & %w[name]).any?\n"  \
      #       '  end'
    end

    # override validate and errors together
    def errors
      validation_result.errors
    end

    def process_request
      record_event
      { guid: attrs[:guid] }
    end

    def valid?
      validate
    end

    def validation_result
      @validation_result ||= validator.call(attrs) # guid: attrs[:guid], name: attrs[:name], slogan: attrs[:slogan])
    end

    def validator
      injected_validator || self.class.preset_validator
    end

    def record_event
      event_recorder.call(guid: attrs[:guid], attrs: attrs, params: params, options: options)
    end

    def event_recorder
      injected_event_recorder || self.class.preset_event_recorder
    end

    class << self
      attr_accessor :custom_event_recorder, :custom_validator

      def event_recorder(custom_event_recorder)
        @custom_event_recorder = custom_event_recorder
      end

      def default_event_recorder
        name.sub('Services', 'Commands')
      end

      def preset_event_recorder
        (custom_event_recorder || default_event_recorder).constantize
      end

      def validator(custom_validator)
        @custom_validator = custom_validator
      end

      def default_validator
        name + 'Validator'
      end

      def preset_validator
        (custom_validator || default_validator).constantize
      end
    end
  end
end
