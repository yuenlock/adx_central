# frozen_string_literal: true

module AdxCentral
  module Models
    class AdxEventListener < AdxCentral::AdxModel
      has_paper_trail

      def info
        attributes.except('lock_version', 'created_at', 'updated_at')
                  .merge(listener: listener_name.constantize)
      end
    end
  end
end
