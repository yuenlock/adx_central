# frozen_string_literal: true

module AdxCentral
  module Models
    class AdxEvent < AdxCentral::AdxModel
      has_paper_trail
    end
  end
end
