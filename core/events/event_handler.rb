# frozen_string_literal: true

module AdxCentral
  class EventHandler
    def self.call(guid:, options: nil)
      new(guid: guid, options: options).process
    end

    attr_reader :guid, :options, :event_query, :injected_cmd_handler
    attr_reader :aggregate_name

    # @custom_cmd_handler = nil

    class << self
      # def accept_event(name:, attributes:)
      #   @accepted_events ||= []
      #   @accepted_events << { name: name, attributes: attributes }
      # end

      # attr_reader :accepted_eventsx

      def command_handler(handler)
        @cmd_handler = handler
      end

      attr_reader :cmd_handler
    end

    def initialize(guid:, options: nil, command_handler: nil, event_query: nil)
      @guid = guid
      @options = options
      @aggregate_name = self.class.name.demodulize.gsub('Handler', '')
      @injected_cmd_handler = command_handler
      @event_query = event_query || AdxCentral::EventQuery
    end

    def process
      apply_events
    end

    # Child class override
    def apply(event)
      run_command JSON.parse(event.data)
      # raise NotImplementedError, 'Please implement this in ' + self.class.name
    end

      # def apply(event)
      #   # data = JSON.parse event.data
      #   # # run_command data
      #   # case event.name
      #   # when 'attrs_changed'
      #   #   run_command(data.slice('name', 'slogan'))
      #   # when 'name_changed'
      #   #   xyz = do_something_with(data.slice('name'))
      #   #   run_command(xyz)
      #   # when 'slogan_changed'
      #   #   pqr = do_something_else_with data.slice('slogon')
      #   #   run_command(smart_slogan: data[:slogan], pqr: pqr)
      #   # else
      #   #   run_command data
      #   # end
      # end


    private

    def cmd_handler
      @cmd_handler ||= (injected_cmd_handler || self.class.cmd_handler || default_cmd_handler).constantize
    end

    def default_cmd_handler
      module_name = self.class.name
                        .sub('Events', 'Commands')
                        .sub(self.class.name.demodulize, 'Update')
      # module_name.sub(self.class.name.demodulize, 'Update').constantize
    end

    def apply_events
      events.each { |ev| apply ev }
    end

    def events
      event_query.where(aggregate: aggregate_name, guid: guid)
    end

    def run_command(attrs)
      cmd_handler.call(guid: guid, attrs: attrs)
    end
  end
end
