# frozen_string_literal: true

module AdxCentral
  class EventListener
    class << self
      attr_reader :guid, :options, :handler_name

      def call(guid:, options: {}, async: true, handler_name: nil)
        @guid = guid
        @options = options.merge(async: async)
        @handler_name = handler_name || name.gsub('Listener', 'Handler')
        async ? add_to_queue : trigger
      end

      def add_to_queue
        EventQueue.perform_later(
          handler_name: handler_name,
          guid: guid, options: options
        )
      end

      def trigger
        handler_name.constantize.call(guid: guid, options: options)
      end
    end
  end
end
