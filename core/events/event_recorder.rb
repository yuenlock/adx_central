# frozen_string_literal: true

module AdxCentral
  # class UpdateEvent
  class EventRecorder
    def self.call(guid:, attrs:, params:, options: {})
      new(guid: guid, attrs: attrs, params: params, options: options).process
    end

    class << self
      def accept_event(name:, attributes:)
        @accepted_events ||= []
        @accepted_events << { name: name, attributes: attributes }
      end

      attr_reader :accepted_events
        # @accepted_events
      # end
    end

    attr_reader :guid, :attrs, :params, :options, :persistor, :listener_query
    attr_reader :aggregate_name
    # attr_reader :accepted_events

    def initialize(guid:, attrs:, params:, options: {}, persistor: nil, listener_query: AdxCentral::ListenerQuery)
      @guid = guid
      @attrs = attrs
      @params = params
      @options = options
      @persistor = persistor || AdxCentral::Persistor
      @aggregate_name = self.class.name.split('::')[-2].singularize
      @listener_query = listener_query
    end

    def process
      before_persist
      persist_changes
      dispatch
      { guid: guid }
    end

    def before_persist
      # to be overridden
    end

    def after_persist
      # to be overridden
    end

    def persist_changes
      event = self.class.accepted_events.select { |ae| ae[:attributes] == attrs.keys.sort }
      persist_event(event_name: event.first[:name], data: attrs) if event.any?
      # raise NotImplementedError, 'Please implement in ' + self.class.name
    end

    def persist_event(event_name:, data:, meta_data: { user_id: 99 })
      persistor.call(aggregate: aggregate_name, guid: guid, event_name: event_name, data: data, meta_data: meta_data)
    end

    def dispatch
      event_listeners.each do |event_listener|
        event_listener[:listener].call(guid: guid, options: options, async: event_listener[:async])
      end
    end

    def event_listeners
      listener_query.where(aggregate: @aggregate_name)
    end
  end
end
