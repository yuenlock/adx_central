# frozen_string_literal: true

module AdxCentral
  class EventQuery
    def self.where(aggregate:, guid:, last_event_id: 0)
      new(aggregate: aggregate, guid: guid, last_event_id: last_event_id).process
    end

    attr_reader :aggregate, :guid, :last_event_id, :event_model

    def initialize(aggregate:, guid:, last_event_id: 0, event_model: nil)
      @aggregate = aggregate
      @guid = guid
      @last_event_id = last_event_id
      @event_model = event_model || Models::AdxEvent
    end

    def process
      event_model.where(aggregate: aggregate, guid: guid)
                 .where('id > ?', last_event_id)
    end
  end
end
