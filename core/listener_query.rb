# frozen_string_literal: true

module AdxCentral
  class ListenerQuery
    def self.where(aggregate:)
      new(aggregate: aggregate).process
    end

    attr_reader :aggregate, :listener_model

    def initialize(aggregate:, listener_model: nil)
      @aggregate = aggregate
      @listener_model = listener_model || Models::AdxEventListener
    end

    def process
      listeners
    end

    def listeners
      listener_records.map(&:info)
    end

    def listener_records
      listener_model.where(aggregate: aggregate)
    end
  end
end
